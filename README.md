# DECO1800

Team DE & Co. project

* Use Web IDE to edit codes and files on GitLab directly; or submit merge requests from your local computer files.
* No need to create a new branch when submitting codes -- Commit to **master** branch directly.
* Project preview is available at https://fzmi.gitlab.io/deco1800/ (may take from a few seconds to hours to update).
* Project code will be uploaded to the team's FTP server (under ./public_html/project/master folder) once needed.

### Files:

* .gitlab-ci.yml - CI/CD file used for generating preview pages automatically (no need to modify).
* index.html - Starting page of the website.

### Links:

* [Private Team Padlet](https://deco1800-2019.padlet.org/yz/w8eztawfzg0f)
* [Public Team Padlet](https://deco1800-2019.padlet.org/yz/q5lm19riirqj)
* [Team Home Page](https://4h.interaction.courses)

### References:

Icons from Icon8.

Code:

puzzle game reference with modification.
background code @kootoopas (https://codepen.io/kootoopas/pen/reyqg)